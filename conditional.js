const ktp = 17;
const keterangan = ktp < 30 ? "Sudah pantas" : "belum pantas";
console.log(keterangan); // Masih muda

const kondisiAnd = ktp < 50 && "Sudah pantas"; // Jika kondisi true, akan mereturn "Sudah pantas"
const kondisiOr = ktp < 15 || "belum pantas"; // Jika kondisi false, akan mereturn "belum pantas"
console.log(kondisiAnd); // sudah pantas
console.log(kondisiOr); // belum pantas

const langgananNetflix = [
  {
    Tempo: "1 Hari",
    harga: 25000
  },
  {
    Tempo: "1 Bulan",
    harga: 100000
  },
  {
    Tempo: "1 tahun",
    harga: 500000
  }
];

const netflix = langgananNetflix.map((primer) => primer.Tempo);
console.log(netflix);
// ["Harry Potter", "Tumbuh Kembang Anak", "Belajar koding dalam 7 hari"]

const netflixMahal = langgananNetflix.filter((primer) => primer.harga > 70000);
console.log(netflixMahal);
